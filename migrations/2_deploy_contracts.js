const Factory = artifacts.require("UniswapV2Factory.sol");
const GovToken1 = artifacts.require("GovToken1.sol");
const GovToken2 = artifacts.require("GovToken2.sol");
// const GToken1 = artifacts.require("GToken1.sol");
// const GToken2 = artifacts.require("GToken2.sol");

module.exports = async function (deployer, netwok, addresses) {
  await deployer.deploy(Factory, addresses[0]);
  const factory = await Factory.deployed();
  let addressToken1, addressToken2;
  if(netwok == 'mainnet'){
    addressToken1 = '';
    addressToken2 = '';
  }else{
    await deployer.deploy(GovToken1, 100000000000000,"TokenGovernorSwap 1",  9, "GT1" );
    await deployer.deploy(GovToken2, 100000000000000,"TokenGovernorSwap 2",  9, "GT2" );
    // await deployer.deploy(GToken4, 1000000000000000000000n, "TokenGovernorSwap 1",  18, "GT4" );
    // await deployer.deploy(GToken5, 1000000000000000000000n,"TokenGovernorSwap 2",  18, "GT5" );
    const token1 = await GovToken1.deployed();
    const token2 = await GovToken2.deployed();
    addressToken1 = token1.address;
    addressToken2 = token2.address;
    console.log(addressToken1, addressToken2);
    await factory.createPair(addressToken1,addressToken2);
    console.log(await factory.getPair(addressToken1,addressToken2));
  }
  
  //agafem el parcode hash?
};
